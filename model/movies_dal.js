var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM movies_view';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getAllAdd = function(callback) {
    var query ='CALL movies_getall()';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(movie_id, callback) {
    var query = 'CALL movie_getbyid(?)';
    var queryData = [movie_id];
    console.log(query);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO movies (title, release_year, summary) VALUES (?, ?, ?)';
    var queryData = [params.title, params.release_year, params.summary];
    connection.query(query, queryData, function(err, result) {
        var movie_id = result.insertId;
        var query = 'INSERT INTO movie_genres (movie_id, genre_id) VALUES ?';
        var movieGenreData = [];
        if (params.genre_id.constructor === Array) {
            for (var i = 0; i < params.genre_id.length; i++) {
                movieGenreData.push([movie_id, params.genre_id[i]]);
            }
        }
        else {
            movieGenreData.push([movie_id, params.genre_id]);
        }
        connection.query(query, [movieGenreData], function(err, result){
            var query = 'INSERT INTO book_to_film (movie_id, book_id) VALUES ?';
            var bookData = [];
            if (params.book_id.constructor === Array) {
                for (var i = 0; i < params.book_id.length; i++) {
                    bookData.push([movie_id, params.book_id[i]]);
                }
            }
            else {
                bookData.push([movie_id, params.book_id]);
            }
            connection.query(query, [bookData], function(err, result) {
                callback(err, result);
            });
        });
    });
};

exports.delete = function(movie_id, callback) {
    var query = 'DELETE FROM movies WHERE movie_id = ?';
    var queryData = [movie_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

var movieGenreInsert = function(movie_id, genreIdArray, callback){
    var query = 'INSERT INTO movie_genres (movie_id, genre_id) VALUES ?';
    var movieGenreData = [];
    if (genreIdArray.constructor === Array) {
        for (var i = 0; i < genreIdArray.length; i++) {
            movieGenreData.push([movie_id, genreIdArray[i]]);
        }
    }
    else {
        movieGenreData.push([movie_id, genreIdArray]);
    }
    connection.query(query, [movieGenreData], function(err, result){
        callback(err, result);
    });
};
module.exports.movieGenreInsert = movieGenreInsert;

var bookInsert = function(movie_id, bookIdArray, callback){
    var query = 'INSERT INTO book_to_film (movie_id, book_id) VALUES ?';
    var bookData = [];
    if (bookIdArray.constructor === Array) {
        for (var i = 0; i < bookIdArray.length; i++) {
            bookData.push([movie_id, bookIdArray[i]]);
        }
    }
    else {
        bookData.push([movie_id, bookIdArray]);
    }
    connection.query(query, [bookData], function(err, result){
        callback(err, result);
    });
};
module.exports.bookInsert = bookInsert;

var movieGenreDeleteAll = function(movie_id, callback){
    var query = 'DELETE FROM movie_genres WHERE movie_id = ?';
    var queryData = [movie_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
module.exports.movieGenreDeleteAll = movieGenreDeleteAll;

var bookDeleteAll = function(movie_id, callback){
    var query = 'DELETE FROM book_to_film WHERE movie_id = ?';
    var queryData = [movie_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
module.exports.bookDeleteAll = bookDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE movies SET title = ?, release_year = ?, summary = ? WHERE movie_id = ?';
    var queryData = [params.title, params.release_year, params.summary, params.movie_id];
    connection.query(query, queryData, function(err, result) {
        movieGenreDeleteAll(params.movie_id, function(err, result){
            movieGenreInsert(params.movie_id, params.genre_id, function(err, result){
                bookDeleteAll(params.movie_id, function(err, result){
                    bookInsert(params.movie_id, params.book_id, function(err, result){
                        callback(err, result);
                    });
                });
            });
        });
    });
};

exports.edit = function(movie_id, callback) {
    var query = 'CALL movie_getinfo(?)';
    var queryData = [movie_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};