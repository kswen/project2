var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM author_view';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getAllAdd = function(callback) {
    var query ='CALL author_getall()';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(author_id, callback) {
    var query = 'CALL author_getbyid(?)';
    var queryData = [author_id];
    console.log(query);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO author (first_name, last_name, biography) VALUES (?, ?, ?)';
    var queryData = [params.first_name, params.last_name, params.biography];
    connection.query(query, queryData, function(err, result) {
        if(params.book_id != null) {
            var author_id = result.insertId;
            var query = 'INSERT INTO book_author (author_id, book_id) VALUES ?';
            var Data = [];
            if (params.book_id.constructor === Array) {
                for (var i = 0; i < params.book_id.length; i++) {
                    Data.push([author_id, params.book_id[i]]);
                }
            }
            else {
                Data.push([author_id, params.book_id]);
            }
            connection.query(query, [Data], function (err, result) {
                callback(err, result);
            });
        }
        else {
            callback(err, result);
        }
    });
};

exports.delete = function(author_id, callback) {
    var query = 'DELETE FROM author WHERE author_id = ?';
    var queryData = [author_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

var bookInsert = function(author_id, bookIdArray, callback){
    var query = 'INSERT INTO book_author (author_id, book_id) VALUES ?';
    var bookData = [];
    if (bookIdArray.constructor === Array) {
        for (var i = 0; i < bookIdArray.length; i++) {
            bookData.push([author_id, bookIdArray[i]]);
        }
    }
    else {
        bookData.push([author_id, bookIdArray]);
    }
    connection.query(query, [bookData], function(err, result){
        callback(err, result);
    });
};
module.exports.bookInsert = bookInsert;

var bookDeleteAll = function(author_id, callback){
    var query = 'DELETE FROM book_author WHERE author_id = ?';
    var queryData = [author_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
module.exports.bookDeleteAll = bookDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE author SET first_name = ?, last_name = ?, biography = ? WHERE author_id = ?';
    var queryData = [params.first_name, params.last_name, params.biography, params.author_id];
    connection.query(query, queryData, function(err, result) {
        bookDeleteAll(params.author_id, function(err, result){
            if(params.book_id != null) {
                bookInsert(params.author_id, params.book_id, function (err, result) {
                    callback(err, result);
                });
            }
            else{
                callback(err, result);
            }
        });
    });
};

exports.edit = function(author_id, callback) {
    var query = 'CALL author_getinfo(?)';
    var queryData = [author_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};