var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM genres_view;';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(genre_id, callback) {
    var query = 'CALL genre_getbyid(?)';
    var queryData = [genre_id];
    console.log(query);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO genres (genre) VALUES (?)';
    var queryData = [params.genre];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};


exports.delete = function(genre_id, callback) {
    var query = 'DELETE FROM genres WHERE genre_id = ?';
    var queryData = [genre_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE genres SET genre = ? WHERE genre_id = ?';
    var queryData = [params.genre, params.genre_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.edit = function(genre_id, callback) {
    var query = 'CALL genres_getinfo(?)';
    var queryData = [genre_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};