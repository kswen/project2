var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM books_view';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getAllAdd = function(callback) {
    var query ='CALL books_getall()';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(book_id, callback) {
    var query = 'CALL book_getbyid(?)';
    var queryData = [book_id];
    console.log(query);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO books (title, release_year, summary) VALUES (?, ?, ?)';
    var queryData = [params.title, params.release_year, params.summary];
    connection.query(query, queryData, function(err, result) {
        var book_id = result.insertId;
        var query = 'INSERT INTO book_genres (book_id, genre_id) VALUES ?';
        var bookGenreData = [];
        if (params.genre_id.constructor === Array) {
            for (var i = 0; i < params.genre_id.length; i++) {
                bookGenreData.push([book_id, params.genre_id[i]]);
            }
        }
        else {
            bookGenreData.push([book_id, params.genre_id]);
        }
        connection.query(query, [bookGenreData], function(err, result){
            var query = 'INSERT INTO book_author (book_id, author_id) VALUES ?';
            var authorData = [];
            if (params.author_id.constructor === Array) {
                for (var i = 0; i < params.author_id.length; i++) {
                    authorData.push([book_id, params.author_id[i]]);
                }
            }
            else {
                authorData.push([book_id, params.author_id]);
            }
            connection.query(query, [authorData], function(err, result){
                if(params.movie_id != null){
                    var query = 'INSERT INTO book_to_film (movie_id, book_id) VALUES ?';
                    var movieData = [];
                    if (params.movie_id.constructor === Array) {
                        for (var i = 0; i < params.movie_id.length; i++) {
                        movieData.push([params.movie_id[i], book_id]);
                        }
                    }
                    else {
                        movieData.push([params.movie_id, book_id]);
                    }
                    connection.query(query, [movieData], function(err, result) {
                        callback(err, result);
                    });
                }
                else{
                    callback(err, result);
                }
            });
        });
    });
};

exports.delete = function(book_id, callback) {
    var query = 'DELETE FROM books WHERE book_id = ?';
    var queryData = [book_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

var bookGenreInsert = function(book_id, genreIdArray, callback){
    var query = 'INSERT INTO book_genres (book_id, genre_id) VALUES ?';
    var bookGenreData = [];
    if (genreIdArray.constructor === Array) {
        for (var i = 0; i < genreIdArray.length; i++) {
            bookGenreData.push([book_id, genreIdArray[i]]);
        }
    }
    else {
        bookGenreData.push([book_id, genreIdArray]);
    }
    connection.query(query, [bookGenreData], function(err, result){
        callback(err, result);
    });
};
module.exports.bookGenreInsert = bookGenreInsert;

var movieInsert = function(book_id, movieIdArray, callback){
    var query = 'INSERT INTO book_to_film (movie_id, book_id) VALUES ?';
    var movieData = [];
    if (movieIdArray.constructor === Array) {
        for (var i = 0; i < movieIdArray.length; i++) {
            movieData.push([movieIdArray[i], book_id]);
        }
    }
    else {
        movieData.push([movieIdArray, book_id]);
    }
    connection.query(query, [movieData], function(err, result){
        callback(err, result);
    });
};
module.exports.movieInsert = movieInsert;

var bookGenreDeleteAll = function(book_id, callback){
    var query = 'DELETE FROM book_genres WHERE book_id = ?';
    var queryData = [book_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
module.exports.bookGenreDeleteAll = bookGenreDeleteAll;

var movieDeleteAll = function(book_id, callback){
    var query = 'DELETE FROM book_to_film WHERE book_id = ?';
    var queryData = [book_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
module.exports.movieDeleteAll = movieDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE books SET title = ?, release_year = ?, summary = ? WHERE book_id = ?';
    var queryData = [params.title, params.release_year, params.summary, params.book_id];
    connection.query(query, queryData, function(err, result) {
        bookGenreDeleteAll(params.book_id, function(err, result){
            bookGenreInsert(params.book_id, params.genre_id, function(err, result){
                movieDeleteAll(params.book_id, function(err, result){
                    if(params.movie_id != null) {
                        movieInsert(params.book_id, params.movie_id, function (err, result) {
                            callback(err, result);
                        });
                    }
                    else {
                        callback(err, result);
                    }
                });
            });
        });
    });
};

exports.edit = function(book_id, callback) {
    var query = 'CALL book_getinfo(?)';
    var queryData = [book_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};