var express = require('express');
var router = express.Router();
var movies_dal = require('../model/movies_dal');
var genres_dal = require('../model/genres_dal');
var author_dal = require('../model/author_dal');


router.get('/all', function(req, res) {
    author_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('author/authorViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.author_id == null) {
        res.send('author_id is null');
    }
    else {
        author_dal.getById(req.query.author_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('author/authorViewById', {'result': result[0], 'book': result[1]});
           }
        });
    }
});


router.get('/add', function(req, res){
    author_dal.getAllAdd(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('author/authorAdd', {'author': result[0], 'book': result[1]});
        }
    });
});


router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('First name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('Last name must be provided.');
    }
    else if(req.query.biography == null) {
        res.send('Biography must be provided.');
    }
    else {
        author_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/author/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.author_id == null) {
        res.send('An author id is required');
    }
    else {
        author_dal.edit(req.query.author_id, function(err, result){
            res.render('author/authorUpdate', {author: result[0][0], book: result[1]});
        });
    }
});


router.get('/update', function(req, res) {
    author_dal.update(req.query, function(err, result){
       res.redirect(302, '/author/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.author_id == null) {
        res.send('author_id is null');
    }
    else {
         author_dal.delete(req.query.author_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/author/all');
             }
         });
    }
});

module.exports = router;
