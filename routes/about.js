var express = require('express');
var router = express.Router();
var total_dal = require('../model/total_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
    total_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('about', { title: 'The Book Was Better', 'result':result });
        }
    });
});


module.exports = router;
