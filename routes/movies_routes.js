var express = require('express');
var router = express.Router();
var movies_dal = require('../model/movies_dal');
var books_dal = require('../model/books_dal');
var genres_dal = require('../model/genres_dal');


router.get('/all', function(req, res) {
    movies_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('movies/movieViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.movie_id == null) {
        res.send('movie_id is null');
    }
    else {
        movies_dal.getById(req.query.movie_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('movies/movieViewById', {'result': result[0], 'genre': result[1], 'book': result[2], 'author': result[3]});
           }
        });
    }
});


router.get('/add', function(req, res){
    movies_dal.getAllAdd(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('movies/movieAdd', {'movie': result[0], 'genre': result[1], 'book': result[2]});
        }
    });
});


router.get('/insert', function(req, res){
    if(req.query.title == null) {
        res.send('Movie title must be provided.');
    }
    else if(req.query.release_year == null) {
        res.send('Release year must be provided.');
    }
    else if(req.query.summary == null) {
        res.send('Plot summary must be provided');
    }
    else if(req.query.genre_id == null) {
        res.send('At least one genre must be selected');
    }
    else if(req.query.book_id == null) {
        res.send('At least one book must be selected');
    }
    else {
        movies_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/movies/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.movie_id == null) {
        res.send('A movie id is required');
    }
    else {
        movies_dal.edit(req.query.movie_id, function(err, result){
            res.render('movies/movieUpdate', {movie: result[0][0], genre: result[1], book: result[2]});
        });
    }
});


router.get('/update', function(req, res) {
    movies_dal.update(req.query, function(err, result){
       res.redirect(302, '/movies/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.movie_id == null) {
        res.send('movie_id is null');
    }
    else {
         movies_dal.delete(req.query.movie_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/movies/all');
             }
         });
    }
});

module.exports = router;
