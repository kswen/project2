var express = require('express');
var router = express.Router();
var total_dal = require('../model/total_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'The Book Was Better' });
});

module.exports = router;
