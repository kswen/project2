var express = require('express');
var router = express.Router();
var movies_dal = require('../model/movies_dal');
var genres_dal = require('../model/genres_dal');
var books_dal = require('../model/books_dal');


router.get('/all', function(req, res) {
    books_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('books/bookViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.book_id == null) {
        res.send('book_id is null');
    }
    else {
        books_dal.getById(req.query.book_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('books/bookViewById', {'result': result[0], 'author':result[1], 'genre': result[2], 'movie': result[3]});
           }
        });
    }
});


router.get('/add', function(req, res){
    books_dal.getAllAdd(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('books/bookAdd', {'book': result[0], 'author': result[1], 'genre': result[2], 'movie': result[3]});
        }
    });
});


router.get('/insert', function(req, res){
    // simple validation
    if(req.query.title == null) {
        res.send('Book title must be provided.');
    }
    else if(req.query.release_year == null) {
        res.send('Release year must be provided.');
    }
    else if(req.query.summary == null) {
        res.send('Plot summary must be provided');
    }
    else if(req.query.genre_id == null) {
        res.send('At least one genre must be selected');
    }
    else if(req.query.author_id == null) {
        res.send('At least one author must be selected');
    }
    else {
        books_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/books/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.book_id == null) {
        res.send('A book id is required');
    }
    else {
        books_dal.edit(req.query.book_id, function(err, result){
            res.render('books/bookUpdate', {book: result[0][0], author: result[1], genre: result[2], movie: result[3]});
        });
    }
});


router.get('/update', function(req, res) {
    books_dal.update(req.query, function(err, result){
       res.redirect(302, '/books/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.book_id == null) {
        res.send('book_id is null');
    }
    else {
         books_dal.delete(req.query.book_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/books/all');
             }
         });
    }
});

module.exports = router;
