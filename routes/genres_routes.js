var express = require('express');
var router = express.Router();
var genres_dal = require('../model/genres_dal');


router.get('/all', function(req, res) {
    genres_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('genres/genreViewAll', { 'result':result });
        }
    });
});


router.get('/', function(req, res){
    if(req.query.genre_id == null) {
        res.send('genre_id is null');
    }
    else {
        genres_dal.getById(req.query.genre_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('genres/genreViewById', {'result': result[0], 'movie': result[1], 'book': result[2], 'author':result[3]});
           }
        });
    }
});


router.get('/add', function(req, res){
    genres_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('genres/genreAdd', {'result': result});
        }
    });
});


router.get('/insert', function(req, res){
    if(req.query.genre == null) {
        res.send('A genre must be provided.');
    }
    else {
        genres_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/genres/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.genre_id == null) {
        res.send('A genre id is required');
    }
    else {
        genres_dal.edit(req.query.genre_id, function(err, result){
            res.render('genres/genreUpdate', { 'genre':result[0][0] });
        });
    }
});

router.get('/update', function(req, res) {
    genres_dal.update(req.query, function(err, result){
       res.redirect(302, '/genres/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.genre_id == null) {
        res.send('genre_id is null');
    }
    else {
         genres_dal.delete(req.query.genre_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/genres/all');
             }
         });
    }
});

module.exports = router;
